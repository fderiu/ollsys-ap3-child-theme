<?php

// Enqueue Ion parent theme CSS file

add_action( 'wp_enqueue_scripts', 'ap3_ion_enqueue_styles' );
function ap3_ion_enqueue_styles() {

	// parent theme css
	$version = '1.123';
    wp_enqueue_style( 'ap3-ion-style', get_template_directory_uri().'/style.css', null, $version );

    // child theme css
    wp_enqueue_style( 'ap3-child-style', get_stylesheet_uri(), null, $version );
}

add_action( 'wp_enqueue_scripts', 'google_fonts_enqueue_styles' );
function google_fonts_enqueue_styles() {
	// child theme css
  wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Titillium+Web:300,400,700', null, $version );
}

add_action( 'wp_enqueue_scripts', 'font_awesome_enqueue_styles' );
function font_awesome_enqueue_styles() {
	// child theme css
  wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', null, $version );
}


// Add your custom functions here
require_once('update.php');